package icc.animales;

/**
 *
 * @author Leonardo Gallo
 * @version 1
 */
public class Tortuga extends Animal{

    int prodHuevos;

    /**
     * Constructor
     * @param nombre se refiere al nombre cientifico de la especie
     * @param clase
     * @param prodHuevos 
     */
    public Tortuga(String nombre, String clase, int prodHuevos) {
	super(nombre, clase);
	this.prodHuevos = prodHuevos;
    }

    /**
     *
     *
     */
    public int getProduccion() {
	return prodHuevos;
    }

}
