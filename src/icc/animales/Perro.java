package icc.animales;

/**
 *
 * @author Leonardo Gallo
 * @version 1
 */
public class Perro extends Animal{

    String raza;

    /**
     * Constructor
     * @param nombre se refiere al nombre cientifico de la especie
     * @param clase
     * @param raza
     */
    public Perro(String nombre, String clase, String raza) {
	super(nombre, clase);
	this.raza = raza;
    }

    /**
     *
     *
     */
    public String getRaza() {
	return raza;
    }

}
