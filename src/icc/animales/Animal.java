package icc.animales;

/**
 * Clase con las caracteristigas comunes entre animales, por esta
 * razón la implementamos usando la palabra reservada 'abstract'
 *
 * @author Leonardo Gallo
 * @version 1
 */
public abstract class Animal {

    /** Variables de clase */
    protected String nombre;
    protected String reino;
    protected String clase;

    /**
     *
     * @param nombre
     * @param clase
     */
    public Animal(String nombre, String clase) {
	this.nombre = nombre;
	reino = "animalia";
	this.clase = clase;
    }

    /**
     *
     *
     */
    public String getNombre() {
	return nombre;
    }

    /**
     *
     *
     */
    public String getReino() {
	return reino;
    }

    /**
     *
     *
     */
    public String getClase() {
	return clase;
    }

}
