package icc.animales;

/**
 *
 * @author Leonardo Gallo
 * @version 1
 */
public class Main {

    /**
     *
     * @param args 
     */
    public static void main(String[] args) {
	Tortuga t1 = new Tortuga("testudines", "Sauropsida", 80);
	Perro p1 = new Perro("canis lupus familiaris", "mammalia", "husky siberiano");
	Animal[] animales = new Animal[10];

	System.out.println("Usando métodos de la clase padre " + p1.getReino());
	animales[0] = t1;
	animales[1] = p1;
	animales[2] = new Tortuga("testudines", "Sauropsida", 80);
	animales[3] = new Perro("canis lupus familiaris", "mammalia", "labrador");

	for (int i = 0; i < animales.length; i++) {
	    if (animales[i] != null && animales[i] instanceof Tortuga) 
		System.out.println(animales[i].getNombre() + " " + animales[i].getClase() + " produccion " + ((Tortuga) animales[i]).getProduccion());
	}

	System.out.println(animales[0].equals(animales[2]));
	
    }
    
}
