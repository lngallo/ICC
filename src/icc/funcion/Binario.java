package icc.funcion;

/**
 *
 * Implementación de funciones matemáticas
 * 
 * @author Leonardo Gallo
 * @version 1
 * @date 19/10/2017
 */
public class Binario {

    /**
     *
     * Constructor
     */
    public Binario() {
    }

    /**
     *
     * Convierte un numero real a binario
     * @return una cadena con el número binario correspondiente
     */
    public String convertirABinario(double numero) {

	String cadena = String.valueOf(numero);     //conversión de un entero a cadena
	String[] elemento = cadena.split("\\.");    //separamos la cadena
	int entero = Integer.parseInt(elemento[0]); //conversión de una cadena a entero, guardamos la parte entera del número
	int decimal = Integer.parseInt(elemento[1]);//conversión de una cadena a entero, guardamos la parte decimal del número

	//aquí va el resto de la implementación

	System.out.println(elemento.length + " " + entero + " . " + decimal);
	
	return "";
    }

    /**
     *
     * Extrae las palabras de una cadena
     * @param cadena 
     * @return un arreglo con las palabras de la cadena dada
     */
    public String[] encontrarPalabras(String cadena) {

	/* mediante una expresión regular y el método split para cadenas, extraemos en un arreglo las palabras */
	String[] palabras = cadena.split("\\W* *\\W"); 
	return palabras;
    }

    /**
     *
     * Método principal, pruebas.
     */
    public static void main(final String[] args) {
	String[] palabra;     //arreglo de palabras
	int i = 0;
	double a = 5.5, b = 3.4;
	i = (int) (a/b);      //guardamos la parte entera de un double

	Binario f = new Binario();

	f.convertirABinario(5.06);

	//imprimimos la lista de palabras
	palabra = f.encontrarPalabras("¡Hola!, ¿como estas? Buena tarde. Yo de 10.");
	System.out.println("palabras = " + palabra.length);
	for (int j = 0; j < palabra.length; j++) {
	    System.out.println(palabra[j]);
	}
    }

    
}
