Ejercicios para el alumno.

Antes de hacer uso de la (posible) solución de la práctica 4
almacenada en el paquete icc.centroIyC:

- Revisa la implementación y comenta correctamente.
- Te toca incluir los getter y setter.
- De ser posible elimina la redundancia de la variable 'casta'.

Nota: la clase Main contiene sólo ejemplos de objetos.
